# @darnet

### A Dual-Stage Attention-Based Recurrent Neural Network for Time Series Prediction

## Tags

`narx`, `deep learning`, `time series`, `python`, `pytorch`, `neural networks`, `lstm`, `attention`

## Introduction

In time series modeling, a nonlinear autoregressive exogenous model (NARX) is a model which has exogenous inputs. This means that the model relates the current value of a time series to both: past values of the same series; and current and past values of the driving (exogenous) series. In addition, the model contains an "error" term which relates to the fact that knowledge of the other terms will not enable the current value of the time series to be predicted exactly.

In this project, a dual-stage attention-based recurrent neural network (DA-RNN) is developed. The presented network based on [this](https://arxiv.org/pdf/1704.02971.pdf) paper contains an input attention mechanism to adaptively extract relevant driving series (a.k.a., input features) at each time step by referring to the previous encoder hidden state. In the second stage, the network uses a temporal attention mechanism to select relevant encoder hidden states across all time steps. With this dual-stage attention scheme, DA-RNN can not only make predictions effectively but can also be easily interpreted. Empirical studies based upon the NASDAQ 100 Stock dataset demonstrate that the implemented model is close to the model, the results of which are presented in the original paper.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes.

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.10 Python 3.6 version;
* PyTorch 0.4.0 version;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── ...
    ├── src                                     # project source
        ├── darrn                               # darrn approach together with additional modules
        ├── datasets                            # pytorch datasets
        ├── modules                             # additional modules of a common purpose
        ├── scripts                             # model related scripts
    ├── config.py
    ├── ...

`/src` has to be marked in PyCharm as sources root folder.


### Data

The NASDAQ 100 Stock dataset consists of 81 major corporations stock prices, which are used as the driving time series. The index value is used as the target series. The frequency of the data collection is minute-by-minute. This data covers the period from July 26, 2016 to December 22, 2016, 105 days in total. Each day contains 390 data points from the opening to closing of the market except that there are 210 data points on November 25 and 180 data points on December 22. In the experiments, the first 35,100 data points are used as the training set and the following 2,730 data points as the validation set. The last 2,730 data points are used as the test set.

The dataset is supposed to be located in `/data` folder and can be downloaded [here](https://yadi.sk/d/v8ygGRWa3aUFnj).

## Approach

### Dual-Stage Attention-Based RNN

In the first stage, the presented network uses attention mechanism to adaptively extract the relevant driving series at each time step by referring to the previous encoder hidden state. In the second stage, a temporal attention mechanism is used to select relevant encoder hidden states across all time steps. These two attention models are well integrated within an LSTM-based recurrent neural network (RNN) and can be jointly trained using standard backpropagation. In this way, the DA-RNN can adaptively select the most relevant input features as well as capture the long-term temporal dependencies of a time series appropriately. The DA-RNN is easy to interpret, and it is robust to noisy inputs.

<p align="center">
  <img src="/uploads/e43584b4cf213783cd84c597a590a218/darrn.png" width="800px" >
</p>

The input attention mechanism computes the attention weights for multiple driving series conditioned on the previous hidden state in the encoder and then feeds the newly computed vector into the encoder LSTM unit. The temporal attention system computes the attention weights based on the previous decoder hidden state and represents the input information as a weighted sum of the encoder hidden states across all the time steps. The generated context vector is then used as an input to the decoder LSTM unit. The output of the last decoder LSTM unit is the predicted result.

### Model parameters

The proposed DA-RNN is smooth and differentiable, so the parameters can be learned by standard backpropagation with mean squared error as the objective function.

In this project, a DA-RNN model with 1-layer LSTM blocks is used for both encoder and decoder.

The hidden state size is 64. The number of time steps in the window is 10.

The following training parameters are used:

* Optimizer: Adam with lr = 0.001;
* Optimizer Scheduler: ReduceLROnPlateau(): {factor=0.9, patience=10};
* Batchsize: 128.

## Running the project

The main file is `main.py` located in the `src/scripts` folder.

It expects a NASDAQ 100 Stock dataset located in the `/data` folder.

### Usage

Training the new model:

```
python main.py train
```

Training the pretrained model:

```
python main.py train --checkpoint filename
```

Evaluate the trained model on random images:

```
python main.py eval --checkpoint filename
```

All optional arguments are defined in `src/scripts/options/parser.py`.

### Results

The model has been trained using GeForce GTX 1080 Ti for ~208 epochs until early stop patience alert. From the plot of loss, we can see that the model is comparable on both train and validation datasets. The jump in the 50th epoch is explained by the peculiarity of the model: the network initially tries to repeat the previous value, since such behavior already gives a good metric and only then it finds a better minimum.
<p align="center">
  <img src="/uploads/e49a6c83e5997d671bdae0c556bfee1c/loss.png" width="400px" >
</p>

In this project, we compare the RMSE on the validation set with the results proposed in the original paper. Since the authors of the original article did not provide complete information (as it usually happens) on data processing and model training, we believe that our results are comparable to the performance in Table 2 of the original paper.

```
INFO:logger:..MSE: 0.15774723
INFO:logger:..RMSE: 0.39717406
```

### Examples

Below are the results of the model predictions on some time intervals are presented along with ground truth values.

<p align="center">
  <img src="/uploads/719d013e4ddd26ef4c8f80213dd1b916/example1.png" width="400px" >
</p>

<p align="center">
  <img src="/uploads/0fc1c535b2f730e736a6925cf797c00b/example2.png" width="400px" >
</p>

<p align="center">
  <img src="/uploads/649934bfcddf793a439c9c96763bb513/example3.png" width="400px" >
</p>
