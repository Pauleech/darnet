import os
import logging
import platform
import multiprocessing
from modules.logering import setup_logger

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# main folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_path = os.path.join(root_path, "data")

# additional folders
models_path = os.path.join(root_path, "src/darnn/models")
scalers_path = os.path.join(root_path, "src/darnn/scalers")

# filenames
nasdaq_filename = os.path.join(data_path, "nasdaq100.csv")
default_scaler = os.path.join(scalers_path, "default.pkl")

# seed
seed = 42

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "darnet.log")
logger = setup_logger("logger", log_name, log_level)

# net params
nasdaq_timesteps = 10
nasdaq_input_size = 81
nasdaq_hidden_size = 64

# training params
nasdaq_batch_size = 128
nasdaq_lr = 0.001
nasdaq_esp = 100
