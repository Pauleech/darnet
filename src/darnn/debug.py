import config
from modules.cuda import cuda
from darnn.decoder import Decoder
from darnn.encoder import Encoder
from modules.transforms import ToTensor
from torch.utils.data import DataLoader
from datasets.nasdaq import NasdaqDataset
from torchvision.transforms import Compose


def main():
    """
    Performs model forward pass for debugging purposes.
    """
    # define nets
    encoder = cuda(Encoder(config.nasdaq_input_size, config.nasdaq_hidden_size, config.nasdaq_timesteps))
    decoder = cuda(Decoder(config.nasdaq_hidden_size, config.nasdaq_hidden_size, config.nasdaq_timesteps))
    # ----
    # define dataset
    dataset = NasdaqDataset(config.nasdaq_filename,
                            timesteps=config.nasdaq_timesteps,
                            transform=Compose([ToTensor()]))
    # ----
    # define data loader
    loader = DataLoader(dataset, batch_size=config.nasdaq_batch_size,
                        shuffle=True, num_workers=config.cores)
    # ----
    for batch_i, batch_sample in enumerate(loader):
        batch_x, batch_y = batch_sample["x"], batch_sample["y_prev"]
        batch_x, batch_y = cuda(batch_x), cuda(batch_y)
        batch_x_encoded = encoder.forward(batch_x)
        y_pred = decoder.forward(batch_x_encoded, batch_y)
        print(y_pred.shape)
        break


if __name__ == "__main__":
    main()
