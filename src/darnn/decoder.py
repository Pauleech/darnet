import torch
import torch.nn.functional as f
from torch import nn


class Decoder(nn.Module):
    def __init__(self, encoder_hidden_size, decoder_hidden_size, timesteps):
        # type: (int, int, int) -> None
        """
        Initiates Decoder object.

        :rtype: None
        :param encoder_hidden_size: dimension of the encoder hidden state.
        :param decoder_hidden_size: dimension of the decoder hidden state.
        :param timesteps: number of time steps.
        :return: None.
        """
        super(Decoder, self).__init__()
        # ----
        self.input_size = encoder_hidden_size
        self.hidden_size = decoder_hidden_size
        self.timesteps = timesteps
        # ----
        # layers
        self.lstm_layer = nn.LSTM(input_size=1, hidden_size=self.hidden_size, num_layers=1)
        self.attn_w = nn.Linear(in_features=2 * self.hidden_size, out_features=self.input_size)
        self.attn_u = nn.Linear(in_features=self.input_size, out_features=self.input_size)
        self.attn_v = nn.Linear(in_features=self.input_size, out_features=1)
        self.fc_combine = nn.Linear(in_features=self.input_size + 1, out_features=1)
        self.fc_final1 = nn.Linear(in_features=self.input_size + self.hidden_size, out_features=self.hidden_size)
        self.fc_final2 = nn.Linear(in_features=self.hidden_size, out_features=1)

    def forward(self, x, y):
        # type: (Tensor, Tensor) -> Tensor
        """
        Forward pass.

        :rtype: Tensor
        :param x: encoded input with size (batch_size, timesteps, input_size).
        :param y: history output with size (batch_size, timesteps).
        :return: predicted scalar value.
        """
        batch_size = x.size(0)
        # ----
        # hidden, cell: (1, batch_size, hidden_size)
        hidden_state = x.new_zeros((1, batch_size, self.hidden_size))
        cell_state = x.new_zeros((1, batch_size, self.hidden_size))
        # context: (batch_size, input_size)
        context = x.new_zeros((batch_size, self.input_size))
        # ----
        for t in range(self.timesteps):
            # eqn.12: W_d * [d, s]
            # ds: (batch_size, timesteps, 2*hidden_size)
            ds = torch.cat((hidden_state.repeat(self.timesteps, 1, 1).permute(1, 0, 2),
                            cell_state.repeat(self.timesteps, 1, 1).permute(1, 0, 2)), dim=2)
            # ----
            # wds: (batch_size * timesteps, input_size)
            wds = self.attn_w(ds.view(-1, 2 * self.hidden_size))
            # ----
            # eqn.12: U_d * h_i
            # uh: (batch_size * timesteps, input_size)
            uh = self.attn_u(x.view(-1, self.input_size))
            # ----
            # eqn.12: v_d * tanh(W_d * [d, s] + U_d * h_i)
            # attn_weights: (batch_size * timesteps, 1)
            attn_weights = self.attn_v(torch.tanh(wds + uh))
            # ----
            # eqn.13: get attention weights
            # attn_weights: (batch_size, timesteps)
            attn_weights = f.softmax(attn_weights.view(-1, self.timesteps), dim=1)
            # ----
            # eqn.14: compute context vector
            # context: (batch_size, input_size)
            context = torch.bmm(attn_weights.unsqueeze(1), x)[:, 0, :]
            # ----
            # eqn.15: compute combination for lstm
            # y_tilde: (batch_size, 1)
            if t < self.timesteps - 1:
                y_tilde = self.fc_combine(torch.cat((context, y[:, t].unsqueeze(1)), dim=1))
                # ----
                # eqn.16: update hidden state with lstm
                self.lstm_layer.flatten_parameters()
                _, lstm_states = self.lstm_layer(y_tilde.unsqueeze(0), (hidden_state, cell_state))
                hidden_state, cell_state = lstm_states
        # eqn.22: final prediction
        y_pred = self.fc_final2(self.fc_final1(torch.cat((hidden_state[0], context), dim=1)))
        return y_pred
