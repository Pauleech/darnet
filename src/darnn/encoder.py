import torch
import torch.nn.functional as f
from torch import nn


class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size, timesteps):
        # type: (int, int, int) -> None
        """
        Initiates Encoder object.

        :rtype: None
        :param input_size: number of exogenous (driving) terms.
        :param hidden_size: dimension of the hidden state.
        :param timesteps: number of time steps.
        :return: None.
        """
        super(Encoder, self).__init__()
        # ----
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.timesteps = timesteps
        # ----
        # layers
        self.lstm_layer = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=1)
        self.attn_w = nn.Linear(in_features=2 * self.hidden_size, out_features=self.timesteps)
        self.attn_u = nn.Linear(in_features=self.timesteps, out_features=self.timesteps)
        self.attn_v = nn.Linear(in_features=self.timesteps, out_features=1)

    def forward(self, x):
        # type: (Tensor) -> Tensor
        """
        Forward pass.

        :rtype: Tensor
        :param x: input_data with size (batch_size, timesteps, input_size).
        :return: encoded input with size (batch_size, timesteps, hidden_size).
        """
        batch_size = x.size(0)
        # ----
        # input_encoded: (batch_size, timesteps, hidden_size)
        input_encoded = x.new_zeros((batch_size, self.timesteps, self.hidden_size))
        # hidden, cell: (1, batch_size, hidden_size)
        hidden_state = x.new_zeros((1, batch_size, self.hidden_size))
        cell_state = x.new_zeros((1, batch_size, self.hidden_size))
        # ----
        for t in range(self.timesteps):
            # eqn.8: W_e * [h, s]
            # hs: (batch_size, input_size, 2*hidden_size)
            hs = torch.cat((hidden_state.repeat(self.input_size, 1, 1).permute(1, 0, 2),
                            cell_state.repeat(self.input_size, 1, 1).permute(1, 0, 2)), dim=2)
            # whs: (batch_size * input_size, timesteps)
            whs = self.attn_w(hs.view(-1, 2 * self.hidden_size))
            # ----
            # eqn.8: U_e * x^k
            # ux: (batch_size * input_size, timesteps)
            ux = self.attn_u(x.permute(0, 2, 1).contiguous().view(-1, self.timesteps))
            # ----
            # eqn.8: v_e * tanh(W_e * [h, s] + U_e * x^k)
            # attn_weights: (batch_size * input_size, 1)
            attn_weights = self.attn_v(torch.tanh(whs + ux))
            # ----
            # eqn.9: get attention weights
            # attn_weights: (batch_size, input_size)
            attn_weights = f.softmax(attn_weights.view(-1, self.input_size), dim=1)
            # ----
            # eqn.10: calculate weighted input
            # input_weighted_t: (batch_size, input_size)
            input_weighted_t = torch.mul(attn_weights, x[:, t, :])
            # ----
            # eqn.11: update hidden state with lstm
            self.lstm_layer.flatten_parameters()
            _, lstm_states = self.lstm_layer(input_weighted_t.unsqueeze(0), (hidden_state, cell_state))
            hidden_state, cell_state = lstm_states
            # ----
            # update output
            input_encoded[:, t, :] = hidden_state
        return input_encoded
