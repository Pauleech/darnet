import os
import torch
import config
import matplotlib.pyplot as plt
from torch import nn
from torch.optim import Adam
from modules.cuda import cuda
from modules.cuda import device
from darnn.encoder import Encoder
from darnn.decoder import Decoder
from torch.utils.data import DataLoader
from modules.logering import setup_logger
from modules.timing import elapsed_timer, timeit
from torch.optim.lr_scheduler import ReduceLROnPlateau
logger = setup_logger(__name__, config.log_name, config.log_level)


class DarNet(object):
    """
    Dual-Stage Attention-Based Recurrent Neural Network
    """

    def __init__(self, input_size, encoder_hidden_size, decoder_hidden_size, timesteps,
                 model_path, name="default", learning_rate=0.001, early_stop_patience=100):
        # type: (int, int, int, int, str, str, float, int) -> None
        """
        Initiates DarNet object.

        :rtype: None
        :param input_size: number of exogenous (driving) terms.
        :param encoder_hidden_size: dimension of the encoder hidden state.
        :param decoder_hidden_size: dimension of the decoder hidden state.
        :param timesteps: number of time steps.
        :param model_path: model path.
        :param name: object name.
        :param learning_rate: model initial learning rate.
        :param early_stop_patience: early stop patience.
        :return: None.
        """
        self.input_size = input_size
        self.encoder_hidden_size = encoder_hidden_size
        self.decoder_hidden_size = decoder_hidden_size
        self.timesteps = timesteps
        self.learning_rate = learning_rate
        # ----
        self.__best_epoch = 0
        self.early_stop_patience = early_stop_patience
        # ----
        self.__model_path = os.path.join(model_path, name)
        if not os.path.exists(self.__model_path):
            os.makedirs(self.__model_path)
        # ----
        # nets
        self.__encoder = cuda(Encoder(self.input_size, self.encoder_hidden_size, self.timesteps))
        self.__decoder = cuda(Decoder(self.encoder_hidden_size, self.decoder_hidden_size, self.timesteps))
        # ----
        # optimizers
        self.__encoder_optimizer = \
            Adam(filter(lambda p: p.requires_grad, self.__encoder.parameters()), lr=learning_rate)
        self.__encoder_scheduler = ReduceLROnPlateau(self.__encoder_optimizer, factor=0.9, patience=10)
        self.__decoder_optimizer = \
            Adam(filter(lambda p: p.requires_grad, self.__decoder.parameters()), lr=learning_rate)
        self.__decoder_scheduler = ReduceLROnPlateau(self.__decoder_optimizer, factor=0.9, patience=10)
        # ----
        # loss
        self.__loss_func = nn.MSELoss()
        self.__best_loss = float("inf")
        self.__losses = {"train": [], "val": []}

    @timeit(logger, "training the model", title=True)
    def train(self, data_loader, num_epochs, start_epoch=0):
        # type: (DataLoader, int, int) -> None
        """
        Trains DarNet model.

        :rtype: None
        :param data_loader: data loader object.
        :param num_epochs: number of training epochs.
        :param start_epoch: number of first training epoch.
        :return: None.
        """
        plt.figure(dpi=300)
        for epoch in range(start_epoch, num_epochs):
            if epoch > (self.__best_epoch + self.early_stop_patience):
                logger.info("..early stopping")
                break
            text = "epoch {:03d}/{:03d}".format(epoch + 1, num_epochs)
            with elapsed_timer(logger, text, title=True):
                for phase in ["train", "val"]:
                    data_loader.dataset.phase = phase
                    if phase == "train":
                        self.__encoder.train(True)
                        self.__decoder.train(True)
                    else:
                        self.__encoder.train(False)
                        self.__decoder.train(False)
                    # ----
                    running_loss = 0.0
                    for batch_i, batch_sample in enumerate(data_loader):
                        # perform batch iteration
                        loss = self._batch_iteration(batch_sample, phase)
                        # statistics
                        running_loss += loss * batch_sample["x"].shape[0]
                    epoch_loss = running_loss / len(data_loader.dataset)
                    self.__losses[phase].append(epoch_loss)
                    logger.info("..{} loss: {:.8f}".format(phase, epoch_loss))
                    # ----
                    if phase == "val":
                        self.__encoder_scheduler.step(epoch_loss)
                        self.__decoder_scheduler.step(epoch_loss)
                        for pg in self.__encoder_optimizer.param_groups:
                            logger.info("..learning rate: {:.8f}".format(pg["lr"]))
                            self.learning_rate = pg["lr"]
                        if epoch_loss < self.__best_loss:
                            self.__best_epoch = epoch
                            self.__best_loss = epoch_loss
                            self.__save_checkpoint(epoch, is_best=True)
                            logger.info("..got new best model")
                        self.__save_checkpoint(epoch)
                # plot loss
                if epoch > 0:
                    self.__plot_loss(epoch)
                # update log file
                self.__update_log(epoch)

    def predict(self, x, y):
        # type: (ndarray, ndarray) -> ndarray
        """
        Returns predicted value for given exogenous (driving) terms and history.

        :rtype: ndarray
        :param x: input_data with size (batch_size, timesteps, input_size).
        :param y: history output with size (batch_size, timesteps).
        :return: predictions with size (batch_size).
        """
        batch_x = cuda(torch.from_numpy(x).type(torch.FloatTensor))
        batch_y = cuda(torch.from_numpy(y).type(torch.FloatTensor))
        batch_x_encoded = self.__encoder.forward(batch_x)
        y_pred = self.__decoder.forward(batch_x_encoded, batch_y)
        return y_pred.data.numpy()

    def _batch_iteration(self, batch_sample, phase):
        # type: (dict, str) -> float
        """
        Performs batch iteration.

        :rtype: float
        :param batch_sample: dataset batch sample.
        :param phase: learning phase: ["train", "val"].
        :return: loss.
        """
        self.__encoder_optimizer.zero_grad()
        self.__decoder_optimizer.zero_grad()
        # ----
        # forward pass
        batch_x, batch_y = cuda(batch_sample["x"]), cuda(batch_sample["y_prev"])
        batch_x_encoded = self.__encoder.forward(batch_x)
        batch_y_pred = self.__decoder.forward(batch_x_encoded, batch_y)
        batch_y_true = cuda(batch_sample["y_next"])
        # ----
        # backward + optimize only if in training phase
        loss = self.__loss_func(batch_y_pred, batch_y_true)
        # ---
        if phase == "train":
            loss.backward()
            self.__encoder_optimizer.step()
            self.__decoder_optimizer.step()
        return loss.item()

    def __plot_loss(self, epoch):
        # type: (int) -> None
        """
        Plots loss function for current epoch.

        :rtype: None
        :param epoch: current epoch.
        :return: None.
        """
        filename = os.path.join(self.__model_path, "loss.png")
        plt.title("Model losses: {} epochs".format(epoch + 1))
        plt.xlabel("epoch")
        plt.ylabel("loss")
        plt.yscale("log")
        plt.plot(range(1, epoch + 2), self.__losses["train"], label="train")
        plt.plot(range(1, epoch + 2), self.__losses["val"], label="validation")
        plt.legend(loc="upper left")
        plt.savefig(filename)
        plt.clf()

    def __update_log(self, epoch):
        # type: (int) -> None
        """
        Updates log.txt file for current epoch.

        :rtype: None
        :param epoch: current epoch.
        :return: None.
        """
        template = "{:03d}\t{:.8f}\t{:.8f}\t{:.8f}\n"
        filename = os.path.join(self.__model_path, "log.txt")
        if not os.path.isfile(filename):
            with open(filename, "w") as f:
                f.write("Epoch\tLR\tTrain Loss\tVal Loss\n")
        with open(filename, "a") as f:
            line = template.format(epoch + 1, self.learning_rate,
                                   self.__losses["train"][-1], self.__losses["val"][-1])
            f.write(line)

    def __save_checkpoint(self, epoch, is_best=False):
        # type: (int, bool) -> None
        """
        Saves checkpoint for current epoch.

        :rtype: None
        :param epoch: current epoch.
        :param is_best: is best model.
        :return: None.
        """
        name = "model_best" if is_best else "checkpoint"
        filename = os.path.join(self.__model_path, "{}.pth.tar".format(name, epoch + 1))
        state = {
            "epoch": epoch + 1,
            "encoder_state_dict": self.__encoder.state_dict(),
            "decoder_state_dict": self.__decoder.state_dict(),
            "encoder_optimizer_state_dict": self.__encoder_optimizer.state_dict(),
            "decoder_optimizer_state_dict": self.__decoder_optimizer.state_dict(),
            "best_loss": self.__best_loss,
            "best_epoch": self.__best_epoch,
            "losses": self.__losses,
        }
        torch.save(state, filename)

    def load_checkpoint(self, filename):
        # type: (str) -> int
        """
        Loads model checkpoint and returns current epoch.

        :rtype: int
        :param filename: filename to load.
        :return: current epoch.
        """
        if device == "cuda":
            checkpoint = torch.load(filename)
        else:
            checkpoint = torch.load(filename, map_location=lambda storage, loc: storage)
        # ----
        start_epoch = checkpoint["epoch"]
        self.__best_loss = checkpoint["best_loss"]
        self.__best_epoch = checkpoint["best_epoch"]
        self.__losses = checkpoint["losses"]
        self.__encoder.load_state_dict(checkpoint["encoder_state_dict"])
        self.__decoder.load_state_dict(checkpoint["decoder_state_dict"])
        self.__encoder_optimizer.load_state_dict(checkpoint["encoder_optimizer_state_dict"])
        self.__decoder_optimizer.load_state_dict(checkpoint["decoder_optimizer_state_dict"])
        # ----
        logger.info("..loading checkpoint trained for {} epochs".format(start_epoch))
        return start_epoch
