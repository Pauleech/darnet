import pandas as pd
from torch.utils.data import Dataset


class NasdaqDataset(Dataset):
    """
    NASDAQ 100 torch Dataset.
    """

    def __init__(self, filename, timesteps, phase="train", transform=None):
        # type: (str, int, str, callable) -> None
        """
        Initiates NasdaqDataset object.

        :rtype: None
        :param filename: nasdaq csv filename.
        :param timesteps: number of time steps.
        :param phase: learning phase: ["train", "val", "test"].
        :param transform: optional transform to be applied on a sample.
        :return: None.
        """
        self.__data = pd.read_csv(filename)
        # ----
        self.timesteps = timesteps
        self.phase = phase
        self.transform = transform
        # ----
        # tab.1: the statistics of two datasets
        self.__train_size = 35100
        self.__val_size = 2730
        self.__test_size = 2730
        # ----
        self.__data_splits = dict()
        self.__set_splits()

    def __set_splits(self):
        # type: () -> None
        """
        Defines train/val/test folds.

        :rtype: None
        :return: None.
        """
        self.__data_splits["train"] = self.__data[:self.__train_size]
        self.__data_splits["val"] = self.__data[self.__train_size:self.__train_size + self.__val_size]
        self.__data_splits["test"] = self.__data[self.__train_size + self.__val_size:]

    @property
    def data(self):
        # type: () -> DataFrame
        """
        Returns data DataFrame for current phase.

        :rtype: DataFrame
        :return: data DataFrame for current phase.
        """
        return self.__data_splits[self.phase]

    def __len__(self):
        # type: () -> int
        """
        Returns dataset length.

        :rtype: int
        :return: dataset length.
        """
        return len(self.data) - self.timesteps - 1

    def __getitem__(self, idx):
        # type: (int) -> dict
        """
        Returns dataset sample as dictionary.

        :rtype: dict
        :param idx: index.
        :return: dataset sample.
        """
        x = self.data.values[:, :-1]
        y = self.data["NDX"].values
        t = self.timesteps
        # ----
        sample = {"x": x[idx: idx + t],
                  "y_prev": y[idx: idx + t - 1],
                  "y_next": y[idx + t - 1: idx + t],
                  "phase": self.phase}
        # ----
        if self.transform:
            sample = self.transform(sample)
        return sample
