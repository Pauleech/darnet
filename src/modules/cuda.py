import torch
from typing import Optional
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def cuda(obj):
    # type: (Optional) -> Optional
    """
    Returns cuda object if cuda is available.

    :rtype: Optional
    :param obj: some object.
    :return: same object or cuda object.
    """
    return obj.to(device)
