import torch
import numpy as np


class ToTensor(object):
    """
    Torch ToTensor custom transform.
    """

    def __call__(self, sample):
        # type: (dict) -> dict
        """
        Converts ndarrays in sample to Tensors.
        
        :rtype: dict
        :param sample: dataset sample.
        :return: dataset sample.
        """
        x, y_prev, y_next = sample["x"], sample["y_prev"], sample["y_next"]
        return {"x": torch.from_numpy(x).type(torch.FloatTensor),
                "y_prev": torch.from_numpy(y_prev).type(torch.FloatTensor),
                "y_next": torch.from_numpy(y_next).type(torch.FloatTensor)}


class Normalize(object):
    """
    Torch normalization custom transform.
    """

    def __init__(self, scaler):
        # type: (object) -> None
        """
        Initiates Normalize object.

        :rtype: None
        :param scaler: scaler object.
        :return: None.
        """
        self.scaler = scaler

    def __call__(self, sample):
        # type: (dict) -> dict
        """
        Applies scaler on sample.

        :rtype: dict
        :param sample: dataset sample.
        :return: dataset sample.
        """
        x, y_prev, y_next = sample["x"], sample["y_prev"], sample["y_next"]
        # ----
        # transform x
        shape = x.shape
        temp = np.zeros((shape[0], shape[1] + 1))
        temp[:, :-1] = x
        x = self.scaler.transform(temp)[:, :-1]
        # ----
        # transform y_prev
        temp = np.zeros((shape[0] - 1, shape[1] + 1))
        temp[:, -1] = y_prev
        y_prev = self.scaler.transform(temp)[:, -1]
        # ----
        # transform y_next
        temp = np.zeros((1, shape[1] + 1))
        temp[:, -1] = y_next
        y_next = self.scaler.transform(temp)[:, -1]
        return {"x": x, "y_prev": y_prev, "y_next": y_next}
