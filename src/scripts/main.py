import os
import config
import numpy as np
import matplotlib.pyplot as plt
from darnn.model import DarNet
from sklearn.externals import joblib
from torch.utils.data import DataLoader
from scripts.options.parser import parser
from datasets.nasdaq import NasdaqDataset
from torchvision.transforms import Compose
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from modules.transforms import ToTensor, Normalize


def train(args):
    # type: (dict) -> None
    """
    Trains model.

    :rtype: None
    :param args: parsed arguments.
    :return: None.
    """
    # define dataset with transforms
    dataset = NasdaqDataset(config.nasdaq_filename, timesteps=args["timesteps"], phase="train")
    # ----
    # define and fit data scaler
    scaler = StandardScaler().fit(dataset.data)
    joblib.dump(scaler, config.default_scaler)
    dataset.transform = Compose([Normalize(scaler), ToTensor()])
    # ----
    # define data loader
    loader = DataLoader(dataset, batch_size=args["batch_size"], shuffle=True, num_workers=config.cores)
    # ----
    # define model
    path = os.path.join(config.models_path)
    model = DarNet(input_size=config.nasdaq_input_size,
                   encoder_hidden_size=args["hidden_size"],
                   decoder_hidden_size=args["hidden_size"],
                   timesteps=args["timesteps"], model_path=path,
                   name=args["model_name"], learning_rate=args["lr"],
                   early_stop_patience=args["esp"])
    # ----
    # train model
    start_epoch = 0
    if args["checkpoint"] is not None:
        start_epoch = model.load_checkpoint(args["checkpoint"])
    model.train(loader, num_epochs=args["num_epochs"], start_epoch=start_epoch)


def evaluate(args):
    # type: (dict) -> None
    """
    Evaluates model.

    :rtype: None
    :param args: parsed arguments.
    :return: None.
    """
    # define dataset with transforms
    dataset = NasdaqDataset(config.nasdaq_filename, timesteps=args["timesteps"], phase="val")
    # ----
    # define and fit data scaler
    scaler = joblib.load(config.default_scaler)
    dataset.transform = Compose([Normalize(scaler), ToTensor()])
    # ----
    # define data loader
    loader = DataLoader(dataset, batch_size=args["batch_size"], shuffle=True, num_workers=config.cores)
    # ----
    # define model
    path = os.path.join(config.models_path)
    model = DarNet(input_size=config.nasdaq_input_size,
                   encoder_hidden_size=args["hidden_size"],
                   decoder_hidden_size=args["hidden_size"],
                   timesteps=args["timesteps"], model_path=path,
                   name=args["model_name"])
    model.load_checkpoint(args["checkpoint"])
    # ----
    # evaluate model
    y_true, y_pred = [], []
    for batch_i, batch_sample in enumerate(loader):
        batch_x, batch_y = batch_sample["x"].numpy(), batch_sample["y_prev"].numpy()
        try:
            y_pred += np.squeeze(model.predict(batch_x, batch_y)).tolist()
            y_true += np.squeeze(batch_sample["y_next"].numpy()).tolist()
        except TypeError:
            y_pred += [np.squeeze(model.predict(batch_x, batch_y))]
            y_true += [np.squeeze(batch_sample["y_next"].numpy())]
    # ----
    # inverse transform
    temp = np.zeros((len(y_pred), config.nasdaq_input_size + 1))
    temp[:, -1] = y_pred
    y_pred = scaler.inverse_transform(temp)[:, -1]
    temp = np.zeros((len(y_true), config.nasdaq_input_size + 1))
    temp[:, -1] = y_true
    y_true = scaler.inverse_transform(temp)[:, -1]
    # ----
    # plotting and printing results
    config.logger.info("..MSE: {:.8f}".format(mean_squared_error(y_true, y_pred)))
    config.logger.info("..RMSE: {:.8f}".format(mean_squared_error(y_true, y_pred)**0.5))
    for i in range(3):
        k = int(np.random.choice(len(y_true) - 20, 1))
        plt.plot(list(range(20)), y_true[k: k + 20], label="ground truth", color="tab:red")
        plt.plot(list(range(20)), y_pred[k: k + 20], label="DA-RNN", color="tab:blue", linestyle='--')
        plt.xlabel("time")
        plt.ylabel("NASDAQ 100")
        plt.title("Model predictions vs ground truth")
        plt.legend(loc="upper left")
        plt.show()


def main():
    """
    Module main function.
    """
    args = parser()
    functions = {"train": train, "eval": evaluate}
    func = functions[args.pop("which", None)]
    func(args)


if __name__ == "__main__":
    main()
