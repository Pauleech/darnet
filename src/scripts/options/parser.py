import config
import argparse


def parser():
    """
    Returns argument dictionary.
    """
    description = "Darnet: dual-stage attention-based Recurrent Neural Network for time series prediction."
    argparser = argparse.ArgumentParser(description=description)
    subparsers = argparser.add_subparsers(help="commands")
    # ----
    train_parser = subparsers.add_parser("train", help="train model")
    train_parser.add_argument("--hidden_size", default=config.nasdaq_hidden_size, type=int,
                              help="encoder and decoder hidden size", required=False)
    train_parser.add_argument("--timesteps", default=config.nasdaq_timesteps, type=int,
                              help="number of serie timesteps", required=False)
    train_parser.add_argument("--model_name", default="default", type=str,
                              help="model name", required=False)
    train_parser.add_argument("--batch_size", default=config.nasdaq_batch_size, type=int,
                              help="batch size", required=False)
    train_parser.add_argument("--lr", default=config.nasdaq_lr, type=float,
                              help="learning rate", required=False)
    train_parser.add_argument("--esp", default=config.nasdaq_esp, type=int,
                              help="early stop patience", required=False)
    train_parser.add_argument("--num_epochs", default=100, type=int,
                              help="number of epochs", required=False)
    train_parser.add_argument("--checkpoint", default=None, type=str,
                              help="checkpoint to load", required=False)
    train_parser.set_defaults(which="train")
    # ----
    eval_parser = subparsers.add_parser("eval", help="evaluate model on the test set")
    eval_parser.add_argument("--hidden_size", default=config.nasdaq_hidden_size, type=int,
                             help="encoder and decoder hidden size", required=False)
    eval_parser.add_argument("--timesteps", default=config.nasdaq_timesteps, type=int,
                             help="number of serie timesteps", required=False)
    eval_parser.add_argument("--batch_size", default=config.nasdaq_batch_size, type=int,
                             help="batch size", required=False)
    eval_parser.add_argument("--model_name", default="default", type=str,
                             help="model name", required=False)
    eval_parser.add_argument("--checkpoint", type=str, help="checkpoint to load", required=True)
    eval_parser.set_defaults(which="eval")
    # ----
    args = vars(argparser.parse_args())
    return args
